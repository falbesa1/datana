# Generated by Django 4.2.8 on 2023-12-05 13:46

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Planilla',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=200)),
                ('descripcion', models.CharField(blank=True, max_length=200, null=True)),
                ('documento', models.FileField(blank=True, null=True, upload_to='uploads/planillas/')),
            ],
            options={
                'ordering': ['nombre'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Planilla_Proyecto',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('planilla', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='planilla_proyecto', to='analisis.planilla')),
            ],
        ),
        migrations.CreateModel(
            name='Proyecto',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=200)),
                ('descripcion', models.CharField(blank=True, max_length=200, null=True)),
                ('planillas', models.ManyToManyField(related_name='planillas', through='analisis.Planilla_Proyecto', to='analisis.planilla')),
            ],
            options={
                'ordering': ['nombre'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Tablas',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=200)),
                ('descripcion', models.CharField(blank=True, max_length=200, null=True)),
                ('proyecto', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='tablas', to='analisis.proyecto')),
            ],
            options={
                'ordering': ['nombre'],
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='planilla_proyecto',
            name='proyecto',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='planilla_proyecto', to='analisis.proyecto'),
        ),
        migrations.CreateModel(
            name='Grafico',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=200)),
                ('descripcion', models.CharField(blank=True, max_length=200, null=True)),
                ('proyecto', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='graficos', to='analisis.proyecto')),
            ],
            options={
                'ordering': ['nombre'],
                'abstract': False,
            },
        ),
    ]
