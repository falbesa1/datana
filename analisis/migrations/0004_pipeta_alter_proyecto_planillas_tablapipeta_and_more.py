# Generated by Django 4.2.8 on 2023-12-18 18:13

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('analisis', '0003_tabla_archivo'),
    ]

    operations = [
        migrations.CreateModel(
            name='Pipeta',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=200)),
                ('descripcion', models.CharField(blank=True, max_length=200, null=True)),
            ],
            options={
                'ordering': ['nombre'],
                'abstract': False,
            },
        ),
        migrations.AlterField(
            model_name='proyecto',
            name='planillas',
            field=models.ManyToManyField(related_name='proyecto', through='analisis.Planilla_Proyecto', to='analisis.planilla'),
        ),
        migrations.CreateModel(
            name='TablaPipeta',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=200)),
                ('descripcion', models.CharField(blank=True, max_length=200, null=True)),
                ('archivo', models.FileField(blank=True, null=True, upload_to='uploads/tablas/')),
                ('pipeta', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='tablas', to='analisis.pipeta')),
            ],
            options={
                'ordering': ['nombre'],
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Planilla_Pipeta',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('pipeta', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='planilla_pipeta', to='analisis.pipeta')),
                ('planilla', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='planilla_pipeta', to='analisis.planilla')),
            ],
        ),
        migrations.AddField(
            model_name='pipeta',
            name='planillas',
            field=models.ManyToManyField(related_name='pipeta', through='analisis.Planilla_Pipeta', to='analisis.planilla'),
        ),
    ]
