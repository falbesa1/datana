# Generated by Django 4.2.8 on 2023-12-05 14:02

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('analisis', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Tablas',
            new_name='Tabla',
        ),
    ]
