from django.shortcuts import render
from plotly.offline import plot
from dash.exceptions import PreventUpdate

import plotly.graph_objects as go
# Create your views here.

import io
import base64
import pandas as pd
import json

import numpy as np

from django.shortcuts import render,get_object_or_404,redirect
#from .models import Coordenadas
#from .forms import CoordinatesForm
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.template import loader
#from .forms import RenewBookForm
#from .forms import CoordinatesForm
import folium
import dash
from dash import dcc, html, dash_table

from django_plotly_dash import DjangoDash
#from . import plotly_app
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output, State, MATCH, ALL
from plotly.tools import mpl_to_plotly
import plotly.graph_objects as go
import plotly.express as px
from dash import no_update
from matplotlib import pyplot as plt
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
                               AutoMinorLocator)
import datetime

from .models import *
from pathlib import Path
from django.core.files import File
from django.conf import settings



app = DjangoDash('Analisis',add_bootstrap_links=True)   # replaces dash.Dash
app.css.append_css({ "external_url" : "/static/css/sb-admin-2.css" })
#app = DjangoDash('SimpleExample')   # replaces dash.Dash
#app.css.append_css({ "external_url" : "/static/assets/css/sb-admin-2.css" })

def suma(operando1,operando2):

    return operando1+operando2

def mulitiplica(operando1,operando2):

    return operando1*operando2


def resta(operando1,operando2):

    return operando1-operando2

def divide(operando1,operando2):

    return operando1/operando2



def Model_options(Items=None):
    
    modelo_options=[]
    for p in Items:
        ###print ("p",p.nombre)
        modelo_options.append({'label': p.nombre, 'value': p.id},)
    return modelo_options
def drop_down(Items=None,titulo=None,ide=None):
    

    menu = html.Div(
                                                                [
                                                                     html.Div(
                                                                [
                                                                html.H6(titulo, style={'margin-right': '2em'}),
                                                                ]
                                                            ),
                                                            html.Div(
                                                                [
                                                                dcc.Dropdown(id=ide, 
                                                                options=[],
                                                                value=None,
                                                                placeholder='-',
                                                                
                                                                ),
                                                                
                                                                ],
                                                                style={'width':'80%','padding': '3px', 'font-size': '12px'},
                                                                id="el-proyecto"),])
    return menu 
                                        

def tarjeta_layout(hijo):
    tarjeta=html.Div(
                        [
                        html.Div(
                                [
                                hijo,
                                ],className="card-body"
                                ),
                        ],className="card border-left-primary shadow  py-2",style={'margin-top': '30px'}
                        )
    return tarjeta
   
def tarjeta_gris(titulo,cuerpo):
    tarjeta=html.Div(
                    [
                    html.Div(
                            [
                            html.H6(titulo,className="m-0 font-weight-bold text-primary",id="Titulo-menu")            
                            ],className="card-header"
                            ),
                    html.Div(
                        [
                            html.Div(
                                    cuerpo,className="card-body"
                                    )       
                        ]
                        )                
                    ],className="card shadow mb-4"
                    )                     
    return tarjeta   
        
def tarjeta_collapse(titulo,cuerpo,ide):
    tarjeta=html.Div(
    [
        dbc.Button(
            titulo,
            id="collapse-button",
            className="mb-3",
            color="primary",
            n_clicks=0,
        ),
        dbc.Collapse(
            dbc.Card(dbc.CardBody(cuerpo)),
            id="collapse",
            is_open=False,
        ),
    ]
)
   
    return tarjeta
def agrega_columna(tabla_id):   
    mas_columnas=html.Div(  
                                  [
                                    html.Div(  
                                            [
                                                dcc.Input(
                                                        id={"type": 'editing-columns-name', "index": tabla_id},
                                                        placeholder='Enter a column name...',
                                                        value='',
                                                        style={'padding': 10}
                                                        ),
                                            ],className="col-lg-12"
                                            ),
                                    html.Div(  
                                            [ html.Button('Add Column', id={"type": 'editing-columns-button', "index": tabla_id}, n_clicks=0)]
                                    ,className="col-lg-12"),
                                    html.Div(  
                                             [
                                            dcc.RadioItems([ 
                                                            {
                                                            "label":
                                                                [
                                                                
                                                                    html.P("Vacio" ),
                                                                ],
                                                            "value": "vacio",
                                                            },
                                                            {
                                                                "label":
                                                                    [
                                                                        
                                                                    html.Div(  [
                                                                                        dcc.Input(
                                                                                                id={"type": 'valor-column-constante', "index": tabla_id},
                                                                                                placeholder='Valor numerico',
                                                                                                value='',
                                                                                                style={'padding': 10}
                                                                                                ),]),
                                                                    ], 
                                                                "value": "constante",
                                                            },
                                                            {
                                                                "label":
                                                                    [
                                                                        
                                                                        html.Div(
                                                                                                                    
                                                                                [
                                                                                dcc.RadioItems(
                                                                                                id={"type": 'column-operacion', "index": tabla_id},
                                                                                                options={
                                                                                                        'igual a': ' igual  ',
                                                                                                        'suma': ' suma  ',
                                                                                                        'resta': ' resta  ',
                                                                                                        'multiplica': ' multip  ',
                                                                                                        'divide': ' div  ',
                                                                                                        'volumen': '  vol  '
                                                                                                },
                                                                                                value='igual',inline=True,
                                                                                                )
                                                                                ],style={'width':'80%','padding': '3px', 'font-size': '12px'},
                                                                                                                    ),
                                                                    ],
                                                                "value": "operacion",
                                                            },
                                                            ],value="vacio" ,id={"type": 'opcion-column-nueva', "index": tabla_id}
                                                        )
                                            ],className="col-lg-12"),
                                    ],className="row")  
    return mas_columnas
        
def menu_columnas(tabla_id,columna_op):
    
    menu_xx=html.Div([
                                html.Div(
                                        [
                                        html.H6("Columna op", style={'margin-right': '2em'}),
                                        ]
                                        ),
                                        html.Div(
                                                [
                                                dcc.Dropdown(
                                                            id={"type": "menu-opera", "index": tabla_id}, 
                                                            options=columna_op,
                                                            
                                                            placeholder='-',
                                                
                                                            ),
                                                
                                                ],style={'width':'80%','padding': '3px', 'font-size': '12px'},
                                                ),
                                html.Div(
                                        [
                                        html.H6("Columna op2", style={'margin-right': '2em'}),
                                        ]
                                        ),
                                html.Div(
                                        [
                                        dcc.Dropdown(id={"type": "menu-opera2", "index": tabla_id}, 
                                        options=columna_op,
                                        
                                        placeholder='-',
                                        
                                        ),
                                        ],style={'width':'80%','padding': '3px', 'font-size': '12px'},
                                        ),
                                ])
    return menu_xx
form_nueva_tabla=html.Div(  
                                  [
                                    html.Div(  
                                            [html.P("Nombre:",style={'padding': 10}),
                                                dcc.Input(
                                                        id="nombre-tabla",
                                                        placeholder='Enter a tabla name...',
                                                        value='',
                                                        style={'padding': 10,'width':'100%'}
                                                        ),
                                            ],className="col-lg-12 d-flex"
                                            ),
                                    html.Div(  
                                            [
                                             
                                                dcc.Input(
                                                        id="filas",
                                                        placeholder='Enter a filas n.',
                                                        value='',
                                                        style={'padding': 10,'width':'100%'}
                                                        ),
                                                html.P(" x "),
                                                dcc.Input(
                                                        id="columnas",
                                                        placeholder='Enter a columnas n.',
                                                        value='',
                                                        style={'padding': 10,'width':'100%'}
                                                        ),
                                            ],className="col-lg-12 d-flex"
                                            ),
                                    
                                    html.Div(  
                                            [ html.Button('Nueva Tabla', id="tablita", className="btn btn-primary")]
                                    ,className="col-lg-12"),
                                    ],
                            className="row")  

tarjeta_analisis=html.Div([tarjeta_gris("Elemento",
                              [drop_down(Items=[],titulo="",ide="Proyecto"),
                              drop_down(Items=Planilla.objects.all(),titulo="Planilla",ide="Planilla")])],className="col-lg-12")

tarjeta_planilla=tarjeta_gris("Area de Planillas",
                              [html.Div([],id="Tablas"),
                               html.Div([],id="Pestania"),
                              html.Div(id='datatable-interactivity-container')])

tarjeta_tablas_mostradas=tarjeta_gris("Area de Tablas",
                              [html.Div([],id="tarjeta-otro-col"),
                                  html.Div([],id="tablas-mostradas"),
                                  html.Div([],id="tabla-nueva-mostrada"),
                                html.Div([],id="grafico-tabla")]
                               )

tarjeta_tabla_nueva=tarjeta_gris("Tabla Nueva",
                              [                                 html.Div([],id="tabla-nueva-mostrada"),
                              ]
                               )

tarjeta_tablas=html.Div([tarjeta_gris("Lista de Tablas",[html.Div([],id="lista-tablas"),]),tarjeta_gris("Nueva Tabla",[html.Div([],id="nueva-tablas"),])],className="col-lg-12")

tarjeta_graficos=html.Div([tarjeta_gris("Graficos",[]),],className="col-lg-12")

fila_de_tarjetas=html.Div([tarjeta_analisis,
                            tarjeta_tablas,
                            
                            
                            tarjeta_graficos],className="row")
app.layout = html.Div(children=[html.Div([ html.Div([fila_de_tarjetas,],className="col-lg-3"),
                                html.Div([tarjeta_planilla,tarjeta_tablas_mostradas,
                                tarjeta_tabla_nueva],className="col-lg-9")],className="row")])

@app.callback(
    Output("collapse", "is_open"),
    [Input("collapse-button", "n_clicks")],
    [State("collapse", "is_open")],
)
def toggle_collapse(n, is_open):
    if n:
        return not is_open
    return is_open

@app.callback(
    [Output(component_id='Planilla', component_property='options'),
    Output(component_id='lista-tablas', component_property='children'),
    Output(component_id='nueva-tablas', component_property='children'),],
    
    
    [Input('Proyecto', "value"),]
    
)
def update_output(proyecto):
    #global numero_elegido
    print ("elige un elemento")
    print (proyecto)
    if proyecto is None:
        raise PreventUpdate
    else:
        el_proyecto=Proyecto.objects.get(pk=proyecto)
        #print ("Proyecto")
        #print (proyecto)
        las_planillas=Proyecto.objects.get(id=proyecto).planillas.all()
        las_tablas=Proyecto.objects.get(id=proyecto).tablas.all()
        planillas_options_var=Model_options(las_planillas)
        #print ("planillas_options_var")
        #print (planillas_options_var)
        lista=[]
        for item in las_tablas:
            #print ("item")
            #print (item.nombre)
            opcion={
            "label": [
                
                html.Span(item.nombre, style={"font-size": 15, "padding-left": 10}),
            ],
            "value":str(item.id) }
            lista.append(opcion)
        lista_tablas=dcc.Checklist(lista,id="lista-de-tablas")
        boton=html.Button("Mostrar Tablas", id="mostrar-tablas",className="btn btn-primary")
        boton2=html.Button("Fusionar Tablas", id="fusionar-tablas",className="btn btn-primary")
        menu_tablas=html.Div([lista_tablas,boton,boton2])
        return [planillas_options_var,menu_tablas,form_nueva_tabla]




@app.callback(
     [Output(component_id='Tablas', component_property='children'),],
    
    Input('Planilla', "value"),
    
)
def update_output(planilla):
    #global numero_elegido

    if planilla is None:
        raise PreventUpdate
    else:
        
        #print ("planilla")
        #print (planilla)
        la_planilla=Planilla.objects.get(id=planilla)
        planilla_actual=pd.ExcelFile(la_planilla.documento)
        opciones_hojas=planilla_actual.sheet_names
        pestanias=[]
        i=0
        for hoja in opciones_hojas:
            i=i+1
            #print ("hoja")
            #print (hoja)
            pestanias.append(dcc.Tab(label=hoja, value=hoja),)
        pestanias_hojas=dcc.Tabs(id="tabs-example-graph", value=None, children=pestanias)
        return [pestanias_hojas]



@app.callback(Output('Pestania', 'children'),
              Input('tabs-example-graph', 'value'),
              State('Planilla', "value"),)
def render_content(tab,planilla):
    if tab is None:
        raise PreventUpdate
    else:
        planilla=Planilla.objects.get(id=planilla)
        planilla_df=pd.read_excel (planilla.documento,sheet_name=tab)
        planilla_df.insert(0,"ID",planilla_df.index)
        
        
        #print ("planilla_df")
        #print (planilla_df)

        tabla_planilla= dash_table.DataTable( planilla_df.to_dict('records'),[{"name": i, "id": i, "deletable": True, "selectable": True,'renamable': True} for i in planilla_df.columns],editable=True,id='datatable-interactivity',
        filter_action="native",
        sort_action="native",
        sort_mode="multi",
        
        column_selectable="multi",
        row_selectable="multi",
        row_deletable=True,
        selected_columns=[],
        selected_rows=[],
        page_action="native",
        page_current= 0,
        page_size= 20,
        )
        boton=html.Button("Agregar esta Tabla", id="nueva-tabla",n_clicks=0,className="btn btn-primary")
        pestania=html.Div([tabla_planilla])
        
        return pestania

@app.callback(
    Output('datatable-interactivity', 'style_data_conditional'),
    Input('datatable-interactivity', 'selected_columns')
)
def update_styles(selected_columns):
    return [{
        'if': { 'column_id': i },
        'background_color': '#D2F3FF'
    } for i in selected_columns]

@app.callback(
    Output('datatable-interactivity-container', "children"),
    Input('datatable-interactivity', "derived_virtual_data"),
    Input('datatable-interactivity', "derived_virtual_selected_rows"),
    Input('datatable-interactivity', 'selected_columns'),
    State('tabs-example-graph', 'value'),
    State('Planilla', "value"),)
def update_graphs(rows, derived_virtual_selected_rows,selected_columns,tab,planilla):
    # When the table is first rendered, `derived_virtual_data` and
    # `derived_virtual_selected_rows` will be `None`. This is due to an
    # idiosyncrasy in Dash (unsupplied properties are always None and Dash
    # calls the dependent callbacks when the component is first rendered).
    # So, if `rows` is `None`, then the component was just rendered
    # and its value will be the same as the component's dataframe.
    # Instead of setting `None` in here, you could also set
    # `derived_virtual_data=df.to_rows('dict')` when you initialize
    # the component.º

    #print ("rows")
    #print (rows)
    #print ("derived_virtual_selected_rows")
    #print (derived_virtual_selected_rows)
    planilla=Planilla.objects.get(id=planilla)
    planilla_df=pd.read_excel (planilla.documento,sheet_name=tab)
    #planilla_df.insert(0,"ID",planilla_df.index)
    if derived_virtual_selected_rows is None:
        derived_virtual_selected_rows = []
    if len(selected_columns) == 0:
        selected_columns=planilla_df.columns
        dff = pd.DataFrame(rows)
        dff = dff [selected_columns]
    else:
        dff = pd.DataFrame(rows)
        dff = dff [selected_columns]
    
    dff.to_csv('tabla_temporal.csv', encoding='utf-8',index=False)
    #dff = planilla_df if rows is None else pd.DataFrame(rows)
    boton=html.Button("Guardar esta Tabla", id="guarda-tabla",className="btn btn-primary")
    tabla_planilla= dash_table.DataTable( dff.to_dict('records'),[{"name": i,"id": i} for i in dff.columns],editable=True,id='datatable-result',page_action="native",
        page_current= 0,
        page_size= 20)
    
    tabla_boton=html.Div([tabla_planilla,html.Div(id='div-guarda-tabla'),boton])
    return tabla_boton
@app.callback( Output('div-guarda-tabla', "children"),
              Input("guarda-tabla","n_clicks"),
            State('Proyecto', "value"),)
def guarda_tabla(value,proyecto):
    if value is None:
        raise PreventUpdate
    else:
        el_proyecto=Proyecto.objects.get(pk=proyecto)
        nueva_tabla=Tabla(nombre=" - PLANILLA")
        nueva_tabla.save()
        nueva_tabla.nombre="T"+str(nueva_tabla.id)+nueva_tabla.nombre
        nueva_tabla.save()
        path = Path('tabla_temporal.csv')
        
        with path.open(mode="rb") as f:
            nueva_tabla.archivo = File(f, name=path.name)
            nueva_tabla.save()
        nueva_tabla.proyecto=el_proyecto
        nueva_tabla.save()

        return html.H1("tabla guardada id:" + str(nueva_tabla.id))

@app.callback( Output('tablas-mostradas', "children"),
              Input("mostrar-tablas","n_clicks"),
              State("lista-de-tablas","value"),
            )
def mostrar_tabla(boton, value):
    print ("muestra tablase????")
    if boton  is None:
        raise PreventUpdate
    else:
        print ("value")
        print (value)
        las_tablas=Tabla.objects.all().filter(pk__in=value)
        lista_tablas=[]
        enlaza_tablas=[]
        for tabla in las_tablas:
            print ("taba: ")
            print (tabla)
            
            tabla_csv=pd.read_csv(tabla.archivo.path)
            enlaza_tablas.append(tabla_csv)
            print ("tabla_csv")
            print (tabla_csv)
            
            

            
            
            columna_op=[]
            for col in tabla_csv.columns:
                columna_op.append({'label': col, 'value':  tabla_csv.columns.get_loc(col)})
            menu_x=html.Div([
                html.Div(
                            [
                            html.H6("Columna", style={'margin-right': '2em'}),
                            ]
                        ),
                html.Div(
                            [
                            dcc.Dropdown(
                                    id={"type": "menu-interactivo", "index": tabla.id}, 
                                    options=columna_op,
                                    value=None,
                                    placeholder='-',
                            
                                    ),
                            
                            ],style={'width':'80%','padding': '3px', 'font-size': '12px'},id="na"),],className="col-lg-2")
            menu_xx=menu_columnas(tabla.id,columna_op)
            mas_columnas=agrega_columna(tabla.id)
            
            tarjeta_nueva_columna=html.Div([tarjeta_gris("Agregar Columna",[mas_columnas,menu_xx]),],id="mas-columnas",className="col-lg-2")
            lista_tablas.append(tarjeta_nueva_columna)
            boton_mostrada=html.Button("Agregar Tabla", id={"type": "guarda-tabla-mostrada", "index": tabla.id},className="btn btn-primary")
            
            boton_estadistica=html.Button("Agregar Tabla", id={"type": "guarda-tabla-estadistica", "index": tabla.id},className="btn btn-primary")
            div_t=html.Div([],id={"type": "tabla-mostrada-guardada", "index": tabla.id})
            div_t_estadistica=html.Div([],id={"type": "tabla-estadistica-guardada", "index": tabla.id})
            
            
            
            lista_tablas.append(html.Div([],id={
                'type': 'tabla-graficada',
                'index': tabla.id
            },className="col-lg-8"))
            tipo_grafico=html.Div(
                            [html.Div(
                            [
                            html.H6("Tipo Grafico", style={'margin-right': '2em'}),
                            ]
                        ),
                            dcc.Dropdown(
                                    id={"type": "tipo-grafico", "index":tabla.id }, 
                                    options=[{'label':"linea",'value':"linea"},{'label':"barra",'value':"barra"},{'label':"dispersion",'value':"dispersion"}],
                                    value=None,
                                    placeholder='-',
                            
                                    ),
                            
                            ],className="col-lg-2")
            rango=dcc.RangeSlider(id={'type': 'range-slider','index':tabla.id},step=int(len(tabla_csv)/100),min=0,max=len(tabla_csv),value=[0,len(tabla_csv)])
            boton_grafica=html.Button('Nueva-instancia', id={"type": 'grafica-button', "index": tabla.id}, n_clicks=0)
            tarjeta_Estadisticas=html.Div([tarjeta_gris("Estadisticas",[]),],id="estadisticas",className="col-lg-2")
            
            tabla_csv_estadistica=pd.DataFrame(columns=tabla_csv.columns)
            if "Estadistica" not in tabla_csv_estadistica.columns:
                tabla_csv_estadistica.insert(0,"Estadistica","-")

                promedio = tabla_csv.mean(numeric_only=True,skipna=True)
                promedio['Estadistica']="Promedio"
                pd_promedio=pd.DataFrame([promedio.to_dict()])
                tabla_csv_estadistica=pd.concat([tabla_csv_estadistica,pd_promedio])

                std = tabla_csv.std(numeric_only=True,skipna=True)
                std['Estadistica']="Desv. Est."
                pd_promedio=pd.DataFrame([std.to_dict()])
                tabla_csv_estadistica=pd.concat([tabla_csv_estadistica,pd_promedio])
                
                maxi = tabla_csv.max(numeric_only=True,skipna=True)
                maxi['Estadistica']="Maximo"
                pd_promedio=pd.DataFrame([maxi.to_dict()])
                tabla_csv_estadistica=pd.concat([tabla_csv_estadistica,pd_promedio])

                mini = tabla_csv.min(numeric_only=True,skipna=True)
                mini['Estadistica']="Minimo"
                pd_promedio=pd.DataFrame([mini.to_dict()])
                tabla_csv_estadistica=pd.concat([tabla_csv_estadistica,pd_promedio])
            tabla_planilla= dash_table.DataTable( 
                    tabla_csv.to_dict('records'),[{"name": i, "id": i,  "deletable": True, "selectable": True,'renamable': True} for i in tabla_csv.columns],editable=True, id={"type": "tabla-interactiva", "index": tabla.id},page_action="native",
                    filter_action="native",
                    sort_action="native",
                    sort_mode="multi",        
                    column_selectable="multi",
                    row_selectable="multi",
                    row_deletable=True,
                    selected_columns=[],
                    selected_rows=[],
                    export_format="csv",
                    page_current= 0,
                    page_size= 10,
                    include_headers_on_copy_paste=True)
            tabla_planilla_estadistica= dash_table.DataTable( 
                    tabla_csv_estadistica.to_dict('records'),[{"name": i, "id": i,  "deletable": True, "selectable": True,'renamable': True} for i in tabla_csv_estadistica.columns],editable=True, id={"type": "tabla-interactiva-estadistica", "index": tabla.id},page_action="native",
                    filter_action="native",
                    sort_action="native",
                    sort_mode="multi",        
                    column_selectable="multi",
                    row_selectable="multi",
                    row_deletable=True,
                    selected_columns=[],
                    selected_rows=[],
                    export_format="csv",
                    page_current= 0,
                    page_size= 10,
                    include_headers_on_copy_paste=True)
            
            tarjeta_para_tabla=tarjeta_gris(html.P(tabla.nombre),[tabla_planilla,div_t, boton_mostrada,tabla_planilla_estadistica,div_t_estadistica,boton_estadistica])
            print ("tabla_csv_estad")
            print (tabla_csv_estadistica)
            tarjeta_eje_x=html.Div([tarjeta_gris("Definir Eje X",[html.Div([menu_x,tipo_grafico,boton_grafica],className="d-flex"),html.Div(rango)]),],className="col-lg-8")
            
            #lista_tablas.append(tarjeta_Estadisticas)
            lista_tablas.append(tarjeta_eje_x)
            
            lista_tablas.append(tarjeta_para_tabla) 
        boton_enlazadas=html.Button("Agregar Tabla", id={"type": "guarda-tabla-enlazada", "index": tabla.id},className="btn btn-primary")    
        tablas_enlazas=pd.concat(enlaza_tablas)
        print ("tablas_enlazas")
        print (tablas_enlazas)

        resultado_tablas_enlazas= dash_table.DataTable( 
                tablas_enlazas.to_dict('records'),[{"name": i, "id": i} for i in tablas_enlazas.columns],editable=True,id={"type": "tabla-enlazada", "index": tabla.id} ,page_action="native",
                
                export_format="csv",
                page_current= 0,
                page_size= 10)
        div_t_enlazadas=html.Div([],id={"type": "tabla-enlazada-guardada", "index": tabla.id})
        lista_tablas.append(html.H1("Tablas enlazadas"))
        lista_tablas.append(resultado_tablas_enlazas) 
        lista_tablas.append(div_t_enlazadas)
        lista_tablas.append(boton_enlazadas)
        
        
        return html.Div(lista_tablas,className="row")

@app.callback(
    Output({'type': 'tabla-interactiva', 'index': MATCH}, 'columns'),
    Output({'type': 'tabla-interactiva', 'index': MATCH}, 'data'),
    
    Input({"type": 'editing-columns-button', "index": MATCH}, 'n_clicks'),
    Input({"type": 'grafica-button', "index": MATCH}, 'n_clicks'),
    State({"type": 'editing-columns-button', "index": MATCH}, 'id'),
    State({'type': 'tabla-interactiva', 'index': MATCH}, 'data'),
    State({"type": 'editing-columns-name', "index": MATCH}, "value"),
    State({'type': 'tabla-interactiva', 'index': MATCH}, 'columns'),
    State({'type': 'opcion-column-nueva', 'index': MATCH}, 'value'),
    State({'type': 'valor-column-constante', 'index': MATCH}, 'value'),
    State({'type': 'column-operacion', 'index': MATCH}, 'value'),
    State({'type': 'menu-opera', 'index': MATCH}, 'value'),
    State({'type': 'menu-opera2', 'index': MATCH}, 'value'),
    State({'type': 'range-slider', 'index': MATCH}, 'value')
    
    )
def update_columns(n_clicks,n_clicks_graf,id,data, value, existing_columns,opcion,constante,operacion,opera1,opera2,rango):

    def densidad(temperatura):
        return temperatura*2
    def volumen(temperatura,masa):
        return densidad(temperatura)+masa

    """
    
    print ("opera1")
    print (opera1)
    print ("opera2")
    print (opera2)"""
    pandas_tabla=pd.DataFrame.from_dict(data)
    columnas=pandas_tabla.columns
    nueva_data=[]
    print ("operacion")
    print (operacion)
    print ("rango")
    print (rango)
    """ if n_clicks_graf > 0:
        i=0
        for fila in data:
            print ("------")
            print (fila)
            if i>rango[0] and i < rango [1]:
                print ("fila")
                print (fila)
                nueva_data.append(fila)
            i=i+1
        return [existing_columns,nueva_data] """
    if n_clicks > 0:
        if opcion =="vacio":
            for fila in data:
                fila[value]=None
                nueva_data.append(fila)
        if opcion == "constante":
            for fila in data:
                fila[value]=constante
                nueva_data.append(fila)
                
        if opcion == "operacion":
            for fila in data:
                if operacion=="suma":
                    fila[value]=fila[columnas[opera1]]+fila[columnas[opera2]]
                if operacion=="resta":
                    fila[value]=fila[columnas[opera1]]-fila[columnas[opera2]]
                if operacion=="multiplica":
                    fila[value]=fila[columnas[opera1]]*fila[columnas[opera2]]
                if operacion=="divide":
                    fila[value]=fila[columnas[opera1]]/fila[columnas[opera2]]
                if operacion=="volumen":
                    fila[value]=volumen(fila[columnas[opera1]],fila[columnas[opera2]])
                nueva_data.append(fila)
        existing_columns.append({
            'id': value, 'name': value,
            'renamable': True, 'deletable': True,"selectable": True,
        })
        columnas_nuevas=[]
        i=0
        for columna in existing_columns:
            columnas_nuevas.append({'label':columna,'value':i})
            i=i+1
        menu_xx=menu_columnas(id['index'],columnas_nuevas)
        mas_columnas=agrega_columna(id['index'])
        tarjeta_nueva_columna=html.Div([tarjeta_gris("Agregar Columna",[mas_columnas,menu_xx]),],id="otras-columnas-mas",className="col-lg-2")
            
        return [existing_columns,nueva_data]
    
        
        
        
            
        
    else:
        print ("no haga nada")
        raise PreventUpdate

@app.callback(
    Output({'type': 'tabla-graficada', 'index': MATCH}, 'children'),
    Input({"type": "tabla-interactiva", "index": MATCH}, "selected_columns"),
    Input({"type": "menu-interactivo", "index": MATCH}, "value"),
    
    Input({"type": "tipo-grafico", "index": MATCH}, "value"),
    Input({"type": 'grafica-button', "index": MATCH}, 'n_clicks'),
    State({'type': 'tabla-interactiva', 'index': MATCH}, 'data'),
    State({'type': 'tabla-interactiva', 'index': MATCH}, 'id'),
    State({'type': 'range-slider', 'index': MATCH}, 'value')
)
def display_output(values,eje_x,tipo_grafico,boton_graf,data,id,rango):
    
    if len(values) == 0 :
        raise PreventUpdate
    else:
        print ("values")
        print (values)
        print ("eje_x")
        print (eje_x)
        #obj_tabla=Tabla.objects.get(pk=id['index'])
        #tabla_csv=pd.read_csv(obj_tabla.archivo.path)
        
        tabla_csv=pd.DataFrame.from_dict(data)
        if eje_x is None:
            eje_x=tabla_csv.columns[0]
        columna_eje_x=tabla_csv.columns[eje_x]
        filtrado=tabla_csv[values][rango[0]:rango[1]]
        if tipo_grafico == "linea":
            fig = px.line(tabla_csv.iloc[rango[0]:rango[1],:], x=columna_eje_x, y=values)
        if tipo_grafico == "barra":
            fig = px.bar(tabla_csv.iloc[rango[0]:rango[1],:], x=columna_eje_x, y=values)
        if tipo_grafico == "dispersion":
            fig = px.scatter(tabla_csv.iloc[rango[0]:rango[1],:], x=columna_eje_x, y=values)
        
        grafico=dcc.Graph(id="graph"+str(id),figure=fig)
        
        
        
        return html.Div(
            [grafico],className="col-lg-12"
        )    
    
@app.callback( Output('tabla-nueva-mostrada', "children"),
              Input("tablita","n_clicks"),
              State("filas","value"),
              State("columnas","value"),
              
            )
def mostrar_tabla(boton,filas,columnas):
    
    if boton is None:
        raise PreventUpdate
    else:
        params = []
        for i in range(int(columnas)):
            params.append("Columna "+str(i+1))
        tabla_nueva=dash_table.DataTable(
        id='table-nueva-simple',
        columns=(
            
            [{'id': p, 'name': p,'renamable': True } for p in params]
        ),
        data=[
            dict( **{param: 0 for param in params})
            for i in range(1, int(filas)+1)
        ],
        editable=True,
        export_headers= 'display',
        export_format="csv",
        persistence=True,


        )
        boton=html.Button("Guardar Tabla", id="guarda-tabla-nueva",className="btn btn-primary")
        div_t=html.Div([],id="tabla-nueva-guardada")
        div_t2=html.Div([],id="tabla-nueva-guardada2")
        tabla_div=html.Div([tabla_nueva,div_t,div_t2,boton])
        return tabla_div
    
#Guardar Tabla NUEVA
@app.callback([Output('tabla-nueva-guardada','children'),
              Output('Proyecto','value'),],
        Input('guarda-tabla-nueva','n_clicks'),
        
        State('table-nueva-simple', "derived_viewport_data"),
        State('table-nueva-simple', "columns"),
        State('Proyecto', "value"),
        State('nombre-tabla','value'))

def col_nueva_tabla(boton,data,col,proyecto,nombre_tabla):
    print ("quiere apdeit")
    if boton is None or boton==0:
        print ("le soba")
        raise PreventUpdate
    else:
        print ("col")
        print (col)
        print ("data")
        print (data)
        
        nueva_data=[]
        nombre_columnas={}
        for cada_col in col:
            nombre_columnas[cada_col["id"]]=cada_col["name"]
        print ("nombre_columnas")
        print (nombre_columnas)
        tabla_nueva=pd.DataFrame.from_dict(data)
        tabla_nueva.rename(columns=nombre_columnas,inplace=True)
        tabla_nueva.to_csv('tabla_nueva_guardada.csv', encoding='utf-8',index=False)
        el_proyecto=Proyecto.objects.get(pk=proyecto)
        nueva_tabla=Tabla(nombre=nombre_tabla)
        nueva_tabla.save()
        nueva_tabla.nombre="T"+str(nueva_tabla.id)+"-NUEVA-"+nueva_tabla.nombre
        nueva_tabla.save()
        path = Path('tabla_nueva_guardada.csv')
        
        with path.open(mode="rb") as f:
            nueva_tabla.archivo = File(f, name=path.name)
            nueva_tabla.save()
        nueva_tabla.proyecto=el_proyecto
        nueva_tabla.save()




        
        print ("tabla_df")
        print (tabla_nueva)
        return [html.H1("tabal: " + str(nueva_tabla.id)),proyecto]


#GUARDAR TABA MOSTRADA
@app.callback([Output({'type': 'tabla-mostrada-guardada', 'index': MATCH},'children'),
   ],
        Input({'type': 'guarda-tabla-mostrada', 'index': MATCH},'n_clicks'),
        
        State({'type': 'tabla-interactiva', 'index': MATCH}, "data"),
        State({'type': 'tabla-interactiva', 'index': MATCH}, "columns"),
        State('Proyecto', "value"),)

def col_nueva_tabla(boton,data,col,proyecto):
    print ("quiere apdeit tablas")
    if boton is None or boton==0:
        print ("le soba tablas")
        raise PreventUpdate
    else:
        print ("col")
        print (col)
        print ("data")
        print (data)
        tabla_nombre='tabla_mostrada_guardada.csv'
        nueva_data=[]
        nombre_columnas={}
        for cada_col in col:
            nombre_columnas[cada_col["id"]]=cada_col["name"]
        print ("nombre_columnas")
        print (nombre_columnas)
        tabla_nueva=pd.DataFrame.from_dict(data)
        tabla_nueva.rename(columns=nombre_columnas,inplace=True)
        tabla_nueva.to_csv(tabla_nombre, encoding='utf-8',index=False)
        
                
        el_proyecto=Proyecto.objects.get(pk=proyecto)
        nueva_tabla=Tabla(nombre=" - EDITADA")
        nueva_tabla.save()
        nueva_tabla.nombre="T"+str(nueva_tabla.id)+nueva_tabla.nombre
        nueva_tabla.save()
        path = Path(tabla_nombre)
        
        with path.open(mode="rb") as f:
            nueva_tabla.archivo = File(f, name=path.name)
            nueva_tabla.save()
        nueva_tabla.proyecto=el_proyecto
        nueva_tabla.save()




        
        print ("tabla_df")
        print (tabla_nueva)
        return [html.H1("tabal: " + str(nueva_tabla.id))]
#GUARDAR TABA estadisi boton_enlazadas
@app.callback([Output({'type': 'tabla-estadistica-guardada', 'index': MATCH},'children'),
   ],
        Input({'type': 'guarda-tabla-estadistica', 'index': MATCH},'n_clicks'),
        
        State({'type': 'tabla-interactiva-estadistica', 'index': MATCH}, "data"),
        State({'type': 'tabla-interactiva-estadistica', 'index': MATCH}, "columns"),
        State('Proyecto', "value"),)

def col_nueva_tabla(boton,data,col,proyecto):
    print ("quiere apdeit tablas")
    if boton is None or boton==0:
        print ("le soba tablas")
        raise PreventUpdate
    else:
        print ("col")
        print (col)
        print ("data")
        print (data)
        tabla_nombre='tabla_mostrada_guardada.csv'
        nueva_data=[]
        nombre_columnas={}
        for cada_col in col:
            nombre_columnas[cada_col["id"]]=cada_col["name"]
        print ("nombre_columnas")
        print (nombre_columnas)
        tabla_nueva=pd.DataFrame.from_dict(data)
        tabla_nueva.rename(columns=nombre_columnas,inplace=True)
        tabla_nueva.to_csv(tabla_nombre, encoding='utf-8',index=False)
        
                
        el_proyecto=Proyecto.objects.get(pk=proyecto)
        nueva_tabla=Tabla(nombre=" - ESTADISTICA ")
        nueva_tabla.save()
        nueva_tabla.nombre="T"+str(nueva_tabla.id)+nueva_tabla.nombre
        nueva_tabla.save()
        path = Path(tabla_nombre)
        
        with path.open(mode="rb") as f:
            nueva_tabla.archivo = File(f, name=path.name)
            nueva_tabla.save()
        nueva_tabla.proyecto=el_proyecto
        nueva_tabla.save()




        
        print ("tabla_df")
        print (tabla_nueva)
        return [html.H1("tabal: " + str(nueva_tabla.id))]
@app.callback([Output({'type': 'tabla-enlazada-guardada', 'index': MATCH},'children'),
   ],
        Input({'type': 'guarda-tabla-enlazada', 'index': MATCH},'n_clicks'),
        
        State({'type': 'tabla-enlazada', 'index': MATCH}, "data"),
        State({'type': 'tabla-enlazada', 'index': MATCH}, "columns"),
        State('Proyecto', "value"),)

def col_nueva_tabla(boton,data,col,proyecto):
    print ("quiere apdeit tablas")
    if boton is None or boton==0:
        print ("le soba tablas")
        raise PreventUpdate
    else:
        print ("col")
        print (col)
        print ("data")
        print (data)
        tabla_nombre='tabla_enlazada_guardada.csv'
        nueva_data=[]
        nombre_columnas={}
        for cada_col in col:
            nombre_columnas[cada_col["id"]]=cada_col["name"]
        print ("nombre_columnas")
        print (nombre_columnas)
        tabla_nueva=pd.DataFrame.from_dict(data)
        tabla_nueva.rename(columns=nombre_columnas,inplace=True)
        tabla_nueva.to_csv(tabla_nombre, encoding='utf-8',index=False)
        
                
        el_proyecto=Proyecto.objects.get(pk=proyecto)
        nueva_tabla=Tabla(nombre="- ENLAZADA - ")
        nueva_tabla.save()
        nueva_tabla.nombre="T"+str(nueva_tabla.id)+nueva_tabla.nombre
        nueva_tabla.save()
        path = Path(tabla_nombre)
        
        with path.open(mode="rb") as f:
            nueva_tabla.archivo = File(f, name=path.name)
            nueva_tabla.save()
        nueva_tabla.proyecto=el_proyecto
        nueva_tabla.save()




        
        print ("tabla_df")
        print (tabla_nueva)
        return [html.H1("tabal: " + str(nueva_tabla.id))]
def home(request):
    #template = loader.get_template('componente_A/index.html')
    #session = request.session

    #demo_count = session.get('SimpleExample', {})

    #ind_use = demo_count.get('ind_use', 0)
    #ind_use = 1
    #demo_count['ind_use'] = ind_use
    #session['SimpleExample'] = demo_count

    context = {'eleccion':0}
    return render(request, 'welcome.html', context)
    #return HttpResponse(template.render ())

def ProyectosView(request):
    #template = loader.get_template('componente_A/index.html')
    #session = request.session

    #demo_count = session.get('SimpleExample', {})

    #ind_use = demo_count.get('ind_use', 0)
    #ind_use = 1
    #demo_count['ind_use'] = ind_use
    #session['SimpleExample'] = demo_count
    
    Items=Articulos.objects.all()
    modelo_options=[]
    for p in Items:
        ###print ("p",p.nombre)
        modelo_options.append({'label': p.nombre, 'value': p.id},)
    #initial='{"Proyecto": {"options":'+ json.dumps(modelo_options)+ '},"Titulo-menu":{"children":"Pipeta"}}'
    initial='{"Proyecto": {"options":'+ json.dumps(modelo_options)+ '}}'
    print ("-------------initial")
    print (initial)
    context = {'eleccion':0}
    context = {'base_url':settings.BASE_URL,'initial_arguments': initial }
    return render(request, 'proyectos.html', context)
    #return HttpResponse(template.render ())

def PipetasView(request):
    #template = loader.get_template('componente_A/index.html')
    #session = request.session

    #demo_count = session.get('SimpleExample', {})

    #ind_use = demo_count.get('ind_use', 0)
    #ind_use = 1
    #demo_count['ind_use'] = ind_use
    #session['SimpleExample'] = demo_count
    
    Items=PipetasM.objects.all()
    modelo_options=[]
    for p in Items:
        ###print ("p",p.nombre)
        modelo_options.append({'label': p.nombre, 'value': p.id},)
    """ menu = html.Div(
                                                                [
                                                                     html.Div(
                                                                [
                                                                html.H6("Pipeta", style={'margin-right': '2em'}),
                                                                ]
                                                            ),
                                                            html.Div(
                                                                [
                                                                dcc.Dropdown(id="Proyecto", 
                                                                options=modelo_options,
                                                                value=None,
                                                                placeholder='-',
                                                                
                                                                ),
                                                                
                                                                ],
                                                                style={'width':'80%','padding': '3px', 'font-size': '12px'},
                                                                id="el-proyecto"),])     """
    #initial='{"Proyecto": {"options":'+ json.dumps(modelo_options)+ '},"Titulo-menu":{"children":"Pipeta"}}'
    initial='{"Proyecto": {"options":'+ json.dumps(modelo_options)+ '}}'
    print (initial)
    context = {'eleccion':0}
    context = {'base_url':settings.BASE_URL,'initial_arguments': initial }
    return render(request, 'pipetas.html', context)
    #return HttpResponse(template.render ())

