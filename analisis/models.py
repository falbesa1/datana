from django.db import models
from django.contrib.auth.models import User



class ClaseBase(models.Model):
    nombre = models.CharField(max_length=200)
    descripcion = models.CharField(max_length=200,null=True,blank=True)
    class Meta():
        abstract=True
        ordering=['nombre',]
    def __str__(self):
        return self.nombre
# Create your models here.
class Planilla(ClaseBase):
    documento =  models.FileField(upload_to ='uploads/planillas/',null = True,blank=True)

class Tipos(ClaseBase):
    class Meta:
        ordering=['nombre',]
    
class Proyecto(ClaseBase):
    planillas = models.ManyToManyField(Planilla,related_name="proyecto",through="Planilla_Proyecto")
    tipo= models.ForeignKey(Tipos, related_name='proyecto',on_delete=models.CASCADE, null=True,blank=True)


class AnalisisTipos(models.Manager):
    
    def get_queryset(self):
        print ("usuario")
        #print (self.request.user.name)
        return super().get_queryset().all().filter(tipo__nombre='Articulo')
    def save(self, *args, **kwargs):
        #set the value of the read_only_field using the regular field
        self.tipo = Tipos.objects.get(nombre='Articulo')
        # call the save() method of the parent
        super(AnalisisTipos, self).save(*args, **kwargs)

class PipetasTipo(models.Manager):

    def get_queryset(self):
        print ("usuario pipeta")
        #print (self.request.user.name)
        return super().get_queryset().all().filter(tipo__nombre='Pipeta')
    """ def save(self, *args, **kwargs):
        #set the value of the read_only_field using the regular field
        print ("el_tipo nate" )
        el_tipo = Tipos.objects.get(nombre='Pipeta')
        print ("el_tipo")
        print (el_tipo)
        self.tipo = el_tipo.id
        # call the save() method of the parent
        super(PipetasTipo, self).save(*args, **kwargs) """

class Articulos(Proyecto):
    objects= AnalisisTipos()
    class Meta:
        proxy = True

    
class PipetasM(Proyecto):
    objects= PipetasTipo()
    class Meta:
        proxy = True

class Planilla_Proyecto(models.Model):
    proyecto= models.ForeignKey(Proyecto, related_name='planilla_proyecto',on_delete=models.CASCADE, null=True)
    planilla= models.ForeignKey(Planilla, related_name='planilla_proyecto',on_delete=models.CASCADE, null=True)


class Tabla(ClaseBase):
    proyecto= models.ForeignKey(Proyecto, related_name='tablas',on_delete=models.CASCADE, null=True)
    archivo =  models.FileField(upload_to ='uploads/tablas/',null = True,blank=True)


class Grafico(ClaseBase):
    proyecto= models.ForeignKey(Proyecto, related_name='graficos',on_delete=models.CASCADE, null=True)
