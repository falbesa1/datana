from django.urls import path
from . import views



urlpatterns = [
    path('', views.home, name='Analisis'),
    path('proyectos', views.ProyectosView, name='Proyectos'),
    path('pipetas', views.PipetasView, name='Pipetas')
] 
