from django.contrib import admin
from .models import * 
from .admin_class.analisis import *
# Register your models here.
admin.site.register(Proyecto,ProyectosAdmin)
admin.site.register(Tipos,TiposAdmin)
admin.site.register(Articulos,ArticuloAdmin)
admin.site.register(PipetasM,PipetaAdmin)


admin.site.register(Planilla,PlanillasAdmin)
admin.site.register(Tabla)
admin.site.register(Grafico)