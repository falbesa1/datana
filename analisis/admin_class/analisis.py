from django.contrib import admin
from ..models import Proyecto,Planilla,Planilla_Proyecto,Tabla,Grafico,Tipos,Articulos,PipetasM

class PlanillasAdmin(admin.ModelAdmin):
    model = Planilla
    
    


class PlanillasInline(admin.TabularInline):
    model = Planilla_Proyecto
    

    extra = 1




class TablasInline(admin.TabularInline):
    model = Tabla
    

    extra = 1


class GraficosInline(admin.TabularInline):
    model = Grafico
    

    extra = 1


class ProyectoAdmin(admin.ModelAdmin):
    model = Proyecto
    exclude = ['tipo']
    inlines = [ PlanillasInline, TablasInline]


class ProyectosAdmin(admin.ModelAdmin):
    model = Proyecto
    
    inlines = [ PlanillasInline, TablasInline]

class PipetaAdmin(ProyectoAdmin):
    model = PipetasM
    
    
    def save_model(self, request, obj, form, change):        
        obj.save()
        form.save_m2m()
        # your custom stuff goes here
    
        el_tipo = Tipos.objects.get(nombre='Pipeta')
        print ("el_tipo")
        print (el_tipo)
        obj.tipo = el_tipo
        
        obj.save()


class ArticuloAdmin(admin.ModelAdmin):
    model = Articulos
    
    def save_model(self, request, obj, form, change):        
        obj.save()
        form.save_m2m()
        # your custom stuff goes here
    
        el_tipo = Tipos.objects.get(nombre='Pipeta')
        print ("el_tipo")
        print (el_tipo)
        obj.tipo = el_tipo
        
        obj.save()

class TiposAdmin(admin.ModelAdmin):
    model = Tipos
    
    

